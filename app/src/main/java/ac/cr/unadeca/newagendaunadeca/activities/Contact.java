package ac.cr.unadeca.newagendaunadeca.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.tfb.fbtoast.FBToast;

import ac.cr.unadeca.newagendaunadeca.R;

public class Contact extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        ImageButton imageButton = findViewById(R.id.goBackC);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView textView = findViewById(R.id.vive_unadeca);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.facebook.com/ViveUnadeca/"));
                startActivity(browserIntent);
            }
        });

        LinearLayout mapsT = findViewById(R.id.locationT);
        ImageView mapsI = findViewById(R.id.location);

        mapsT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maps();
            }
        });

        mapsI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maps();
            }
        });

        LinearLayout phoneT = findViewById(R.id.phone_num);
        ImageView phoneI = findViewById(R.id.phone_icon);

        phoneT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phone();
            }
        });

        phoneI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                phone();
            }
        });

        LinearLayout emailT = findViewById(R.id.email);
        ImageView emailI = findViewById(R.id.email_icon);

        emailT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email();
            }
        });

        emailI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email();
            }
        });

        LinearLayout whatsappT = findViewById(R.id.whatsapp_num);
        ImageView whatsappI = findViewById(R.id.whatsapp_icon);

        whatsappT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatsapp();
            }
        });

        whatsappI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                whatsapp();
            }
        });
    }

    public void phone() {
        String num = "+50624363300";
        Intent i = new Intent(Intent.ACTION_DIAL);
        i.setData(Uri.fromParts("tel", num, null)) ;
        try {
            startActivity(i);
        } catch (Exception e) {
            FBToast.errorToast(Contact.this, "No se pudo acceder al telefono", FBToast.LENGTH_SHORT);
        }
    }

    public void email() {
        String email = "admisiones@unadeca.net";

        Uri uri = Uri.parse("mailto:" + email)
                .buildUpon()
                .build();

        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, uri);
        try {
            startActivity(Intent.createChooser(emailIntent, ""));
        } catch (Exception e) {
            FBToast.errorToast(Contact.this, "No se pudo acceder al correo", FBToast.LENGTH_SHORT);
        }
    }

    public void whatsapp() {
        String contact = "+50663316083";
        String url = "https://api.whatsapp.com/send?phone=" + contact;

        String action;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        try {
            startActivity(i);
        } catch (Exception e) {
            FBToast.errorToast(Contact.this, "No se pudo acceder al whatsApp", FBToast.LENGTH_SHORT);
        }
    }

    public void maps() {
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=Adventist+University+of+Central+America, Alajuela, Costa+Rica");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            try {
                startActivity(mapIntent);
            } catch (Exception e) {
                FBToast.errorToast(Contact.this, "No se pudo acceder a Google Maps", FBToast.LENGTH_SHORT);
            }
        }
    }

}