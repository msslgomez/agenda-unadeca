package ac.cr.unadeca.newagendaunadeca.database;

import com.raizlabs.android.dbflow.annotation.Database;

@Database(name = agendaUDatabase.dbname, version = agendaUDatabase.dbversion)
        public class agendaUDatabase {
        public static final String dbname = "agendaUDatabase";
        public static final int dbversion = 1;
        }
