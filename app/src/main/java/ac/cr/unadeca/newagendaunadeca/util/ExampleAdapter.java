package ac.cr.unadeca.newagendaunadeca.util;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ac.cr.unadeca.newagendaunadeca.R;
import ac.cr.unadeca.newagendaunadeca.database.models.Events;

import java.util.List;

import static android.content.ContentValues.TAG;

public class ExampleAdapter extends RecyclerView.Adapter<ExampleAdapter.ExampleViewHolder> {

    private List<Events> mEvents;
    private Funtions funt;
    private OnSelectListener mOnSelectListener;

    public ExampleAdapter(List<Events> exampleList, OnSelectListener onSelectListener) {
        mEvents = exampleList;
        this.mOnSelectListener = onSelectListener;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.example_item, null, false);
        return new ExampleViewHolder(v, mOnSelectListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        Events currentItem = mEvents.get(position);

        //if its a one day event will only display that day
        //and if not will display the range like this X - X
        int iDate = Integer.parseInt(funt.formatDateToDay(currentItem.init_date));
        int eDate = Integer.parseInt(funt.formatDateToDay(currentItem.end_date));

        if (iDate == eDate) {
            holder.mTextViewDate.setText(funt.formatDateToDay(currentItem.init_date));
            holder.mTextViewTitle.setText(currentItem.title);
        } else {
            holder.mTextViewDate.setText(funt.formatDateToDay(currentItem.init_date) + " - " + funt.formatDateToDay(currentItem.end_date));
            holder.mTextViewTitle.setText(currentItem.title);
        }
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    public class ExampleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnSelectListener mOnSelectListener;

        public TextView mTextViewDate, mTextViewTitle;

        public ExampleViewHolder(@NonNull View itemView, OnSelectListener OnSelectListener) {
            super(itemView);
            mTextViewDate = itemView.findViewById(R.id.text_view_date);
            mTextViewTitle = itemView.findViewById(R.id.text_view_title);

            mOnSelectListener = OnSelectListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: " + getAdapterPosition());
            //mOnSelectListener.OnSelectClick(getAdapterPosition());
            mOnSelectListener.OnSelectClick(mEvents.get(getAdapterPosition()).id);
        }
    }

    public interface OnSelectListener {
        void OnSelectClick(int position);
    }
}