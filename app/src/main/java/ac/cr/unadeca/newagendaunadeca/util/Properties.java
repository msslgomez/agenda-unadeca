package ac.cr.unadeca.newagendaunadeca.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Properties {

    SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "FirstTime";
    private static final String PREF_MONTH = "MonthView";

    private static final String FirstTime = "firstTime";
    private static final String MonthView = "monthView";
    private static final String Downloaded = "download";

    // Constructor
    public Properties(Context context){
        this._context = context;
        preferences = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = preferences.edit();
    }

    public Boolean getFirstTime() {
        return preferences.getBoolean(FirstTime, true);
    }

    public void setFirstTime(Boolean firstT){
        // Storing url in pref
        editor.putBoolean(FirstTime, firstT);
        // commit changes
        editor.commit();
    }

    public Boolean getDownloaded() {
        return preferences.getBoolean(Downloaded, false);
    }

    public void setDownloaded(Boolean downloaded){
        // Storing url in pref
        editor.putBoolean(Downloaded, downloaded);
        // commit changes
        editor.commit();
    }

    public Boolean getMonthView() {
        return preferences.getBoolean(MonthView, true);
    }

    public void setMonthView(Boolean monthV){
        // Storing url in pref
        editor.putBoolean(MonthView, monthV);
        // commit changes
        editor.commit();
    }


}