package ac.cr.unadeca.newagendaunadeca.activities;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.tfb.fbtoast.FBToast;

import java.util.List;

import ac.cr.unadeca.newagendaunadeca.R;
import ac.cr.unadeca.newagendaunadeca.database.models.Events;
import ac.cr.unadeca.newagendaunadeca.database.models.Events_Table;
import ac.cr.unadeca.newagendaunadeca.util.Funtions;
import ac.cr.unadeca.newagendaunadeca.util.SearchAdapter;

public class Search extends AppCompatActivity implements
        SearchView.OnQueryTextListener,
        SearchAdapter.OnSelectListener {

    private RecyclerView mRecyclerView;
    private SearchAdapter mSearchAdapter;
    private Funtions funt;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setRecycler();

        SearchView searchView = findViewById(R.id.search_view);
        searchView.setOnQueryTextListener(this);

        ImageButton imageButton = (ImageButton) findViewById(R.id.goBack);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goBack();
            }
        });

        TextView textView = findViewById(R.id.vive_unadeca);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(
                        Intent.ACTION_VIEW,
                        Uri.parse("https://www.facebook.com/ViveUnadeca/"));
                startActivity(browserIntent);
            }
        });
    }

    public void showDialogEvent(Activity activity, int id) {
        final Dialog dl = new Dialog(activity);
        dl.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dl.setCancelable(true);
        dl.setContentView(R.layout.dialog_event);

        TextView tvdate = dl.findViewById(R.id.text_event_day);
        TextView tvmonth = dl.findViewById(R.id.text_event_month);
        TextView tvtitle = dl.findViewById(R.id.text_event_title);
        TextView tvdesc = dl.findViewById(R.id.text_event_description);

        Events listado = SQLite.select().from(Events.class)
                .where(Events_Table.id.eq(id))
                .querySingle();

        int iDate = Integer.parseInt(funt.formatDateToDay(String.valueOf(listado.init_date)));
        int eDate = Integer.parseInt(funt.formatDateToDay(String.valueOf(listado.end_date)));

        if (iDate == eDate) {
            tvdate.setText(funt.formatDateToDay(String.valueOf(listado.init_date)));
        } else {
            tvdate.setText(funt.formatDateToDay(String.valueOf(listado.init_date))
                    + " - " + funt.formatDateToDay(String.valueOf(listado.end_date)));
        }

        tvmonth.setText(funt.formatDateMonthSp(listado.init_date));
        tvtitle.setText(listado.title);
        tvdesc.setText(listado.description);

        Button btn_ok = (Button) dl.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dl.dismiss();
            }
        });
        dl.show();
    }

    @Override
    public void OnSelectClick(int id) {
        showDialogEvent(this, id);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        mSearchAdapter.getFilter().filter(newText);
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    public void goBack() {
        finish();
    }

    public void setRecycler() {
        mRecyclerView = findViewById(R.id.reclycer_view_search);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        String firstDate = "2020-01-01 00:00:00";
        String endDate = "2021-01-31 00:00:00";

        List<Events> listado = SQLite.select().from(Events.class)
                .where(Events_Table.end_date.greaterThanOrEq(firstDate))
                .and(Events_Table.init_date.lessThanOrEq(endDate))
                .orderBy(Events_Table.init_date.asc())
                .queryList();

        for (int i = 0; i < 100; i++) {
            mSearchAdapter = new SearchAdapter(listado, this);
            mRecyclerView.setAdapter(mSearchAdapter);
        }
    }

}