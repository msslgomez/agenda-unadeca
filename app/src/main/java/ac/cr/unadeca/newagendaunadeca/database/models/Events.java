package ac.cr.unadeca.newagendaunadeca.database.models;

import ac.cr.unadeca.newagendaunadeca.database.agendaUDatabase;
import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.util.Locale;

@Table(database = agendaUDatabase.class)
public class Events extends BaseModel {
    @Column
    @PrimaryKey(autoincrement = false)
    public int id = 1;

    @Column
    public String init_date;

    @Column
    public String end_date;

    @Column
    public String title;

    @Column
    public String description;

    @Column
    public String color_code;

    @Column
    public int all_day;

    public String toString() {
        return String.format(Locale.getDefault(), this.init_date, this.end_date, this.title);
    }
}