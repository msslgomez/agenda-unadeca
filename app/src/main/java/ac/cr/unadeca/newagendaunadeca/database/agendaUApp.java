package ac.cr.unadeca.newagendaunadeca.database;

import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

public class agendaUApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        FlowManager.init(this);
    }
}
