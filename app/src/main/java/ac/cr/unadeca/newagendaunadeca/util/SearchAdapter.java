package ac.cr.unadeca.newagendaunadeca.util;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ac.cr.unadeca.newagendaunadeca.R;
import ac.cr.unadeca.newagendaunadeca.database.models.Events;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ExampleViewHolder>
        implements Filterable {

    private List<Events> mEvents;
    private List<Events> mEventsFull;
    private List<Events> mfilteredList;
    private OnSelectListener mOnSelectListener;


    private Funtions funt;

    public SearchAdapter(List<Events> exampleList, OnSelectListener onSelectListener) {
        mEvents = exampleList;
        mEventsFull = new ArrayList<>(exampleList);
        this.mOnSelectListener = onSelectListener;
    }

    @NonNull
    @Override
    public ExampleViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_item, null, false);
        return new ExampleViewHolder(v, mOnSelectListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ExampleViewHolder holder, int position) {
        Events currentItem = mEvents.get(position);
        //if its a one day event will only display that day
        //and if not will display the range like this X - X
        int iDate = Integer.parseInt(funt.formatDateToDay(currentItem.init_date));
        int eDate = Integer.parseInt(funt.formatDateToDay(currentItem.end_date));

        if (iDate == eDate) {
            holder.mTextViewDate.setText(funt.formatDateToDay(currentItem.init_date));
            holder.mTextViewMonth.setText(funt.formatDateMonthSp(currentItem.init_date));
            holder.mTextViewTitle.setText(currentItem.title);
        } else {
            holder.mTextViewDate.setText
                    (funt.formatDateToDay(currentItem.init_date) + " - " + funt.formatDateToDay(currentItem.end_date));
            holder.mTextViewMonth.setText(funt.formatDateMonthSp(currentItem.init_date));
            holder.mTextViewTitle.setText(currentItem.title);
        }
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    public class ExampleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnSelectListener mOnSelectListener;

        public TextView mTextViewDate, mTextViewMonth, mTextViewTitle;

        public ExampleViewHolder(@NonNull View itemView, OnSelectListener OnSelectListener) {
            super(itemView);
            mTextViewDate = itemView.findViewById(R.id.text_view_date);
            mTextViewMonth = itemView.findViewById(R.id.text_view_month);
            mTextViewTitle = itemView.findViewById(R.id.text_view_title);

            mOnSelectListener = OnSelectListener;

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Log.d(TAG, "onClick: " + getAdapterPosition());
            mOnSelectListener.OnSelectClick(mEvents.get(getAdapterPosition()).id);
        }
    }

    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            ArrayList<Events> filteredList = new ArrayList<>();


            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mEventsFull);

            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();

                for (Events events : mEventsFull) {
                    String sdate = events.init_date.toLowerCase();
                    String title = events.title.toLowerCase();

                    if (title.contains(filterPattern) || sdate.contains(filterPattern)) {
                        filteredList.add(events);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mEvents.clear();
            mEvents.addAll((List) results.values);
            notifyDataSetChanged();

        }
    };

    public interface OnSelectListener {
        void OnSelectClick(int position);
    }

}