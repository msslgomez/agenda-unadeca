package ac.cr.unadeca.newagendaunadeca.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.ToggleButton;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.github.ybq.android.spinkit.sprite.Sprite;
import com.github.ybq.android.spinkit.style.DoubleBounce;
import com.google.android.material.snackbar.Snackbar;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.leinardi.android.speeddial.SpeedDialActionItem;
import com.leinardi.android.speeddial.SpeedDialView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.raizlabs.android.dbflow.sql.language.Delete;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.json.JSONArray;
import org.json.JSONObject;
import org.threeten.bp.DayOfWeek;
import org.threeten.bp.LocalDate;
import org.threeten.bp.temporal.TemporalAdjusters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ac.cr.unadeca.newagendaunadeca.R;
import ac.cr.unadeca.newagendaunadeca.database.models.Events;
import ac.cr.unadeca.newagendaunadeca.database.models.Events_Table;
import ac.cr.unadeca.newagendaunadeca.decorators.EventDecorator;
import ac.cr.unadeca.newagendaunadeca.decorators.HighlightWeekendsDecorator;
import ac.cr.unadeca.newagendaunadeca.decorators.MySelectorDecorator;
import ac.cr.unadeca.newagendaunadeca.decorators.TodayDecorator;
import ac.cr.unadeca.newagendaunadeca.util.Connectivity;
import ac.cr.unadeca.newagendaunadeca.util.ExampleAdapter;
import ac.cr.unadeca.newagendaunadeca.util.Funtions;
import ac.cr.unadeca.newagendaunadeca.util.Properties;

public class MainActivity extends AppCompatActivity implements
        OnDateSelectedListener,
        ExampleAdapter.OnSelectListener,
        OnMonthChangedListener {

    private CoordinatorLayout coordinatorLayout;
    private RecyclerView mRecyclerView;
    private ExampleAdapter mExampleAdapter;
    private RequestQueue mRequestQueue;
    private Properties properties;
    private Connectivity connect;
    private Funtions funt;
    private MaterialCalendarView widget;
    private ImageButton imageButton;
    private ImageView image;
    private ProgressBar progressBar;
    Calendar c = Calendar.getInstance();
    ArrayList<String> singleE = new ArrayList<>();
    ArrayList<String> rangeE = new ArrayList<>();
    ArrayList<String> rang = new ArrayList<>();

    CalendarDay calendarDay = CalendarDay.today();
    ArrayList<Events> mEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialBuild();
        checkStatus_Download();
        buildCalendarView();
        setRecyclerView();
    }

    private void deleteAndDownloadJSON() {
        Delete.table(Events.class);
        properties.setFirstTime(true);
        checkStatus_Download();
    }

    private void initialBuild() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        AndroidThreeTen.init(this);

        coordinatorLayout = findViewById(R.id.transition_container);
        progressBar = findViewById(R.id.spin_kit);
        Sprite doubleBounce = new DoubleBounce();
        progressBar.setIndeterminateDrawable(doubleBounce);
        progressBar.setVisibility(View.GONE);

        image = findViewById(R.id.imageback);

        imageButton = findViewById(R.id.dwn_button);
        imageButton.setColorFilter(getResources().getColor(R.color.Yellow));
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressBar.setVisibility(View.VISIBLE);
                showDialogDownload(MainActivity.this);
            }
        });

        imageButton = findViewById(R.id.reload_button);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context;
                Animation aniFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
                mRecyclerView.startAnimation(aniFade);
                reload();
            }
        });

        final SpeedDialView speedDialView = findViewById(R.id.fab);
        speedDialView.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_search, R.drawable.ic_search)
                        .setLabel(R.string.search)
                        .setLabelColor(getResources().getColor(R.color.white))
                        .setLabelBackgroundColor(getResources().getColor(R.color.MediumBlue))
                        .setFabBackgroundColor(getResources().getColor(R.color.MediumBlue))
                        .create()
        );

        speedDialView.addActionItem(
                new SpeedDialActionItem.Builder(R.id.fab_contact, R.drawable.ic_contact)
                        .setLabel(R.string.contact)
                        .setLabelColor(getResources().getColor(R.color.white))
                        .setLabelBackgroundColor(getResources().getColor(R.color.MediumBlue))
                        .setFabBackgroundColor(getResources().getColor(R.color.MediumBlue))
                        .create()
        );

        speedDialView.setOnActionSelectedListener(new SpeedDialView.OnActionSelectedListener() {
            @Override
            public boolean onActionSelected(SpeedDialActionItem actionItem) {
                switch (actionItem.getId()) {
                    case R.id.fab_search:
                        openSearch();
                        speedDialView.close();
                        return true;
                    case R.id.fab_contact:
                        openContact();
                        speedDialView.close();
                        return true;
                }

                return true;
            }
        });

        ToggleButton toggle = findViewById(R.id.toggleButton);
        toggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    widget.state().edit().setCalendarDisplayMode(CalendarMode.WEEKS).commit();
                    properties.setMonthView(false);
                    image.setImageResource(0);
                    weeks();
                } else {
                    widget.state().edit().setCalendarDisplayMode(CalendarMode.MONTHS).commit();
                    properties.setMonthView(true);
                    image.setImageResource(R.drawable.fade_logo);
                    reload();
                }
            }
        });

        properties = new Properties(this);

        widget = findViewById(R.id.calendarView);
        widget.setOnDateChangedListener(this);

        mRecyclerView = findViewById(R.id.reclycer_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    public void openSearch() {
        String action;
        Intent intent = new Intent(this, Search.class);
        startActivity(intent);
    }

    public void openContact() {
        String action;
        Intent intent = new Intent(this, Contact.class);
        startActivity(intent);
    }

    public void showDialogDownload(Activity activity) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_download);

        Button btn_yes = (Button) dialog.findViewById(R.id.btn_yes);
        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteAndDownloadJSON();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void showDialogEvent(Activity activity, int id) {
        final Dialog dl = new Dialog(activity);
        dl.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dl.setCancelable(true);
        dl.setContentView(R.layout.dialog_event);

        TextView tvdate = dl.findViewById(R.id.text_event_day);
        TextView tvmonth = dl.findViewById(R.id.text_event_month);
        TextView tvtitle = dl.findViewById(R.id.text_event_title);
        TextView tvdesc = dl.findViewById(R.id.text_event_description);

        Events listado = SQLite.select().from(Events.class)
                .where(Events_Table.id.eq(id))
                .querySingle();

        int iDate = Integer.parseInt(funt.formatDateToDay(String.valueOf(listado.init_date)));
        int eDate = Integer.parseInt(funt.formatDateToDay(String.valueOf(listado.end_date)));

        if (iDate == eDate) {
            tvdate.setText(funt.formatDateToDay(String.valueOf(listado.init_date)));
        } else {
            tvdate.setText(funt.formatDateToDay(String.valueOf(listado.init_date))
                    + " - " + funt.formatDateToDay(String.valueOf(listado.end_date)));
        }

        tvmonth.setText(funt.formatDateMonthSp(listado.init_date));
        tvtitle.setText(listado.title);
        tvdesc.setText(listado.description);

        Button btn_ok = (Button) dl.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dl.dismiss();
            }
        });
        dl.show();
    }

    private void buildCalendarView() {
        widget.setDynamicHeightEnabled(true);
        widget.setSelectedDate(CalendarDay.today());
        properties.setMonthView(true);



        widget.state().edit()
                .setMinimumDate(CalendarDay.from(2020, 1, 1))
                .setMaximumDate(CalendarDay.from(2021, 1, 31))
                .commit();

        widget.removeDecorators();

        widget.addDecorators(
                new MySelectorDecorator(this),
                new HighlightWeekendsDecorator(),
                new TodayDecorator(this)
        );

        widget.invalidateDecorators();
        widget.setOnMonthChangedListener(this);

        widget.setDateTextAppearance(R.style.CustomCalendarDaysText);
        widget.setWeekDayTextAppearance(R.style.CustomDayLabelsText);
        widget.setHeaderTextAppearance(R.style.CustomHeadText);
    }

    private void downloadJSON() {
        mRequestQueue = Volley.newRequestQueue(this);
        String url = "https://calendario.unadeca.ac.cr/data";

        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("events");
                            int size = jsonArray.length();
                            Events events;

                            for (int i = 0; i < size; i++) {

                                JSONObject obj = jsonArray.getJSONObject(i);

                                events = new Events();
                                events.id = obj.getInt("id");
                                events.init_date = obj.getString("init_date");
                                events.end_date = obj.getString("end_date");
                                events.description = obj.getString("description");
                                events.title = obj.getString("title");
                                events.color_code = obj.getString("color_code");
                                events.all_day = obj.getInt("all_day");
                                events.save();
                                System.out.println("downloading (" + i + ") ...");
                            }

                            setRecyclerView();
                            properties.setDownloaded(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                            properties.setDownloaded(false);
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                Snackbar.make(coordinatorLayout, R.string.error, Snackbar.LENGTH_INDEFINITE ).show();
                progressBar.setVisibility(View.GONE);
                properties.setDownloaded(false);
            }
        });

        mRequestQueue = Volley.newRequestQueue(this);
        mRequestQueue.start();
        mRequestQueue.add(request);
    }

    private void getDates(List<Events> listado) {
        List<Events> eventlist = listado;

        int size = eventlist.size();
        int id = 0;

        for (int i = 0; i < size; i++) {
            String st = eventlist.get(i).init_date;
            String ed = eventlist.get(i).end_date;
            id++;

            SimpleDateFormat ogF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
            SimpleDateFormat newF = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
            String fStartDate = null;
            String fEndDate = null;
            Date startD = null;
            Date endD = null;

            try {
                startD = ogF.parse(st);
                endD = ogF.parse(ed);

                fStartDate = newF.format(startD);
                fEndDate = newF.format(endD);

            } catch (ParseException e) {
                e.printStackTrace();
            }


//            System.out.println("start " + fStartDate + " end " + fEndDate);

            if (fStartDate.equals(fEndDate)) {
//                System.out.println("true");
                singleE.add(fStartDate);
            } else if (fStartDate != fEndDate) {
//                System.out.println("false" + fStartDate + " - " + fEndDate);

                LocalDate stD = LocalDate.parse(fStartDate);
                LocalDate edD = LocalDate.parse(fEndDate);

                List<LocalDate> totalD = new ArrayList<>();

                while (!stD.isAfter(edD)) {
                    String stdt = stD.toString();
                    rang.add(stdt);
                    totalD.add(stD);
                    stD = stD.plusDays(1);

                    rangeE.addAll(rang);

                }
//                System.out.println(totalD);
//                System.out.println("range " + rangeE);
            }
        }
//        System.out.println("size: " + rang.size() + "range total " + rang);
        setEvent(singleE);
        setEvent(rang);
        reload();
    }

    private void checkStatus_Download() {
        //properties.setFirstTime(true);
        System.out.println(properties.getFirstTime());
        System.out.println(properties.getDownloaded());
        if (properties.getFirstTime() || !properties.getDownloaded()) {
            if (Connectivity.hasConnectivity(this, true)) {
                downloadJSON();

                System.out.println("in checkstatus");
//                properties.setFirstTime(false);
                //System.out.println("downloaded");
            } else {
                System.out.println("not online");
                setRecyclerView();
            }
        } else {
            System.out.println("first time false or downloaded is true");
        }
        //System.out.println("already downloaded");
    }

    private void setRecyclerView() {

        LocalDate date = LocalDate.now();
        LocalDate ftD = date.with(TemporalAdjusters.firstDayOfMonth());
        String firstD = ftD.toString();
        String firstDate = firstD + " 00:00:00";

        LocalDate edD = date.with(TemporalAdjusters.lastDayOfMonth());
        String endD = edD.toString();
        String endDate = endD + " 23:00:00";

        List<Events> listado = SQLite.select().from(Events.class)
                .where(Events_Table.end_date.greaterThanOrEq(firstDate))
                .and(Events_Table.init_date.lessThanOrEq(endDate))
                .orderBy(Events_Table.init_date.asc())
                .queryList();


        mExampleAdapter = new ExampleAdapter(listado, this);
        mRecyclerView.setAdapter(mExampleAdapter);
        getDates(listado);
    }

    void setList(LocalDate st, LocalDate ed) {
        String firstD = st.toString();
        String firstDate = firstD + " 00:00:00";
        String endD = ed.toString();
        String endDate = endD + " 23:00:00";

        List<Events> listado = SQLite.select().from(Events.class)
                .where(Events_Table.end_date.greaterThanOrEq(firstDate))
                .and(Events_Table.init_date.lessThanOrEq(endDate))
                .orderBy(Events_Table.init_date.asc())
                .queryList();

        mExampleAdapter = new ExampleAdapter(listado, this);
        mRecyclerView.setAdapter(mExampleAdapter);
    }

    void setEvent(List<String> dateList) {
        List<LocalDate> localDateList = new ArrayList<>();

        for (String string : dateList) {
            LocalDate calendar = getLocalDate(string);
            if (calendar != null) {
                localDateList.add(calendar);
            }
        }

//        List<CalendarDay> datesLeft = new ArrayList<>();
//        List<CalendarDay> datesCenter = new ArrayList<>();
//        List<CalendarDay> datesRight = new ArrayList<>();

        List<CalendarDay> datesIndependent = new ArrayList<>();

        for (LocalDate localDate : localDateList) {

            datesIndependent.add(CalendarDay.from(localDate));

//            boolean right = false;
//            boolean left = false;
//            for (LocalDate day1 : localDateList) {
//                if (localDate.isEqual(day1.plusDays(1))) {
//                    left = true;
//                }
//                if (day1.isEqual(localDate.plusDays(1))) {
//                    right = true;
//                }
//            }

//            if (left && right) {
//                datesCenter.add(CalendarDay.from(localDate));
//            } else if (left) {
//                datesLeft.add(CalendarDay.from(localDate));
//            } else if (right) {
//                datesRight.add(CalendarDay.from(localDate));
//            } else {
//                datesIndependent.add(CalendarDay.from(localDate));
//            }

        }
//        setDecor(datesCenter, R.drawable.mb_center);
//        setDecor(datesLeft, R.drawable.mb_left);
//        setDecor(datesRight, R.drawable.mb_right);
        setDecor(datesIndependent, R.drawable.squoval_solid);
    }

    void setDecor(List<CalendarDay> calendarDayList, int drawable) {
        widget.addDecorators(new EventDecorator(MainActivity.this
                , drawable
                , calendarDayList));
    }

    LocalDate getLocalDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date input = sdf.parse(date);
            Calendar cal = Calendar.getInstance();
            cal.setTime(input);
            return LocalDate.of(cal.get(Calendar.YEAR),
                    cal.get(Calendar.MONTH) + 1,
                    cal.get(Calendar.DAY_OF_MONTH));

        } catch (NullPointerException e) {
            return null;
        } catch (ParseException e) {
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSelected(@NonNull MaterialCalendarView materialCalendarView, @NonNull CalendarDay date, boolean b) {
        Context context;
        Animation aniFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        mRecyclerView.startAnimation(aniFade);

        final String text = b ? date.getDate().toString() : "No Selection";
        LocalDate dt = (LocalDate) LocalDate.parse(text);
        String dat = dt.toString();
        String firstDate = dat + " 00:00:00";
        String endDate = dat + " 23:00:00";

        List<Events> listado = SQLite.select().from(Events.class)
                .where(Events_Table.end_date.between(firstDate).and(endDate))
                .or(Events_Table.init_date.between(firstDate).and(endDate))
                .or(Events_Table.end_date.greaterThanOrEq(firstDate))
                .and(Events_Table.init_date.lessThanOrEq(endDate))
                .queryList();

        mExampleAdapter = new ExampleAdapter(listado, this);
        mRecyclerView.setAdapter(mExampleAdapter);
    }

    @Override
    public void onMonthChanged(MaterialCalendarView widget, CalendarDay calendarDay) {
        Context context;
        Animation aniFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        mRecyclerView.startAnimation(aniFade);

        if (properties.getMonthView() == true) {
            String dateS = calendarDay.getDate().toString();

            LocalDate date = LocalDate.parse(dateS);
            LocalDate ftD = date.with(TemporalAdjusters.firstDayOfMonth());
            String firstD = ftD.toString();
            String firstDate = firstD + " 00:00:00";

            LocalDate edD = date.with(TemporalAdjusters.lastDayOfMonth());
            String endD = edD.toString();
            String endDate = endD + " 23:00:00";

            List<Events> listado = SQLite.select().from(Events.class)
                    .where(Events_Table.end_date.greaterThanOrEq(firstDate))
                    .and(Events_Table.init_date.lessThanOrEq(endDate))
                    .orderBy(Events_Table.init_date.asc())
                    .queryList();

            mExampleAdapter = new ExampleAdapter(listado, this);
            mRecyclerView.setAdapter(mExampleAdapter);
            getDates(listado);

        } else if (properties.getMonthView() == false) {
            LocalDate date = calendarDay.getDate();

            LocalDate sun = date.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY));
            LocalDate sat = date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));

            setList(sun, sat);
        }
    }

    public void weeks() {
        String dateS = funt.formatCalendarDayToString(widget.getCurrentDate());
        LocalDate date = LocalDate.parse(dateS);

        LocalDate sun = date.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY));
        LocalDate sat = date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));

        setList(sun, sat);
    }

    public void reload() {
        Context context;
        Animation aniFade = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        mRecyclerView.startAnimation(aniFade);

        if (progressBar == null){
            progressBar.setVisibility(View.VISIBLE);
        }

        if (properties.getMonthView()) {
            String dateS = funt.formatCalendarDayToString(widget.getCurrentDate());
            LocalDate date = LocalDate.parse(dateS);

            LocalDate ftD = date.with(TemporalAdjusters.firstDayOfMonth());
            LocalDate edD = date.with(TemporalAdjusters.lastDayOfMonth());

            setList(ftD, edD);

        } else if (!properties.getMonthView()) {
            String dateS = funt.formatCalendarDayToString(widget.getCurrentDate());
            LocalDate date = LocalDate.parse(dateS);

            LocalDate sun = date.with(TemporalAdjusters.previousOrSame(DayOfWeek.SUNDAY));
            LocalDate sat = date.with(TemporalAdjusters.nextOrSame(DayOfWeek.SATURDAY));

            setList(sun, sat);
        }
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnSelectClick(int id) {
        showDialogEvent(this, id);
    }
}