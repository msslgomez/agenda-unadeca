package ac.cr.unadeca.newagendaunadeca.decorators;

import android.app.Activity;
import android.graphics.drawable.Drawable;

import ac.cr.unadeca.newagendaunadeca.R;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import org.threeten.bp.LocalDate;

/**
 * Decorate a day by making the text big and bold
 */
public class TodayDecorator implements DayViewDecorator {

    private CalendarDay date;
    private Drawable drawable;

    public TodayDecorator(Activity context) {
        date = CalendarDay.today();
        drawable = context.getResources().getDrawable(R.drawable.selector_onedaydecorator);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return date != null && day.equals(date);
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setSelectionDrawable(drawable);
    }

    /**
     * We're changing the internals, so make sure to call {@linkplain MaterialCalendarView#invalidateDecorators()}
     */
    public void setDate(LocalDate date) {
        this.date = CalendarDay.from(date);
    }
}