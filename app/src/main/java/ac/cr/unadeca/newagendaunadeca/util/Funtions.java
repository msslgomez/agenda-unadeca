package ac.cr.unadeca.newagendaunadeca.util;

import androidx.recyclerview.widget.RecyclerView;

import com.prolificinteractive.materialcalendarview.CalendarDay;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Funtions {

    private ExampleAdapter mExampleAdapter;
    private RecyclerView mRecyclerView;
    private CalendarDay calendarDay;

    public static String formatCalendarDayToString(CalendarDay calendarDay) {
        return String.format(Locale.getDefault(), "%04d-%02d-%02d", calendarDay.getYear(), calendarDay.getMonth(), calendarDay.getDay());
    }

    public static String formatDateMonthSp(String date) {
        DateFormatSymbols sym = DateFormatSymbols.getInstance(new Locale("es","ar"));
        sym.setShortMonths(new String[]{"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic" });

        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = null;
        try {
            Date nD = originalFormat.parse(date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM yyyy", sym);

            formattedDate = fmtOut.format(nD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String formatDateMonthSpLong(String date) {
        DateFormatSymbols sym = DateFormatSymbols.getInstance(new Locale("es","ar"));
        sym.setShortMonths(new String[]{"Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic" });

        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = null;
        try {
            Date nD = originalFormat.parse(date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMMM yyyy", sym);

            formattedDate = fmtOut.format(nD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String formatDateToDay(String date) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = null;
        try {
            Date nD = originalFormat.parse(date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("dd");

            formattedDate = fmtOut.format(nD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String formatDateToMonth(String date) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = null;
        try {
            Date nD = originalFormat.parse(date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM");

            formattedDate = fmtOut.format(nD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String formatDateToMonth_Year(String date) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = null;
        try {
            Date nD = originalFormat.parse(date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("MMM - yyyy");

            formattedDate = fmtOut.format(nD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String formatDateToYear(String date) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = null;
        try {
            Date nD = originalFormat.parse(date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy");

            formattedDate = fmtOut.format(nD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }

    public static String formatDate(String date) {
        SimpleDateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formattedDate = null;
        try {
            Date nD = originalFormat.parse(date);
            SimpleDateFormat fmtOut = new SimpleDateFormat("yyyy-MM-dd");

            formattedDate = fmtOut.format(nD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedDate;
    }
}